import React, {Component} from 'react';


const {width,height}=Dimensions.get('screen');

const backAction =  StackActions.reset({
    index: 0,
    key: null,
    actions: [
    NavigationActions.navigate({routeName: 'StackNavi' })
    ]
    })
export default class Products extends Component{

    state ={
        name:'',
        Description:'',
        isLoggingIn:false,
        message:'',
    }
    handleValueChange(code) {
        this.setState({ code })
      } 
    
    _userLogin = () => {

        this.setState({ isLoggingIn: true, message: '' });

        var params = {
            email: this.state.email,
            password: this.state.password
        };

        var formBody = [];
        for (var property in params) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(params[property]);
            formBody.push(encodedKey + ":" + encodedValue);
        }
        formBody = formBody.join("&");

        var proceed = false;
        var response = null;
        fetch('http://localhost/snap_cart_backend/public/index.php/api/get_products', {
                method: "POST",
                headers: {
                    'Accept':'applicaton/json',
                    'Content-Type':'application/json',
                    'Authorization':(Platform.OS==='ios')?Environment.IosBasicToken:Environment.AndroidBasicToken,
                    'client-id':(Platform.OS==='ios')?'hilianspa-app-ios':'hilianspa-app-android',

                },
                body: JSON.stringify({
                  email: params.email,
                  password :params.password,
                })
            })
            .then((response) => response.json())
            .then((response) => {
                if (response.status==true) { 
                    proceed = true;
                     var data = response.response;
                     // console.log(data);
                     var accesstoken = data.access_token;
                     response=null;
                    // // console.log(role);
                    AsyncStorage.removeItem('accesstoken').then(() =>AsyncStorage.setItem('accesstoken',accesstoken));
                    // this.setState({'role': roler });
                }
                else {
                    this.setState({message:response.error.message});
                    response=null;
            }
            })
            .then(() => {
                
                // AsyncStorage.getItem('role').then((value) => this.setState({'role': value }));
                this.setState({ isLoggingIn: false })
                if (proceed) {
                    this.setState({isLoggedIn: true});
                    
                    this.props.navigation.dispatch(backAction);
                    // this.props.navigation.dispatch(this.reset());
                    
                }
            })
            .catch(err => {
				this.setState({ message: err.message });
				this.setState({ isLoggingIn: false })
			});
    }

    clearEmail = () => {
        // this._email.setNativeProps({ text: '' });
        this.setState({ message: '' });
    }

    clearPassword = () => {
        this._password.setNativeProps({ text: '' });
        this.setState({ message: '' });
    }



