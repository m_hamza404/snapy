import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,Image,ImageBackground,alignItems,resizeMode,styles,Dimensions,TextInput,TouchableOpacity} from 'react-native';
import Profile from './Profile';
import App from './App';
import thirdpage from './thirdpage';
import SignUp from './SignUp';
import {StackNavigator,TabNavigator} from 'react-navigation';
import previouscart from './previouscart';
import newcart from './newcart';
import VisitorScreen from './VisitorScreen';
import Oproductlist from './onlineprolist';
import MCartlist from './MCartlist';
import MPreviouscart from './MPreviouscart';
import MNewcart from './MNewcart';
import ScannerScreen from './ScannerScreen';
import MProductslist from './MProductslist';
import Scanner from './Scanner';
import Opencam from './Opencam';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});



const Application = StackNavigator({
    Home: { screen: App },
    Profile: { screen: Profile},
    thirdpage:{screen: thirdpage},
    MProductslist:{screen:MProductslist},
    SignUp:{screen: SignUp},
    ScannerScreen:{screen:ScannerScreen},
    VisitorScreen:{screen:VisitorScreen},
    previouscart:{screen :previouscart},
    Oproductlist:{screen:Oproductlist},
    MPreviouscart:{screen:MPreviouscart},
    MProductslist:{screen:MProductslist},
    MNewcart:{screen:MNewcart},
    Scanner:{screen:Scanner},
    
  });

  export const Tab=TabNavigator({
    previouscart:{screen:()=><TabNav/>},
    newcart:{screen:newcart}

  });

    export const TabNav=StackNavigator({
    previouscart:{screen :previouscart},
    Oproductlist:{screen:Oproductlist},
    Scanner:{screen:Scanner}

  });
  
  export const Tabmart=TabNavigator({
    MPreviouscart:{screen:()=><TabNav2/>},
    MNewcart:{screen:()=><TabNav3/>}
    
  });
  
  export const TabNav2=StackNavigator({
    MPreviouscart:{screen:MPreviouscart},
    MProductslist:{screen:MProductslist},
    Scanner:{screen:Scanner}


  });
  export const TabNav3=StackNavigator({
    MNewcart:{screen:MNewcart},
    Opencam:{screen:Opencam},
    Scanner:{screen:Scanner}


  });
 
export default class Home extends Component {
    render() {
      return (
       <Application/>
      );
    }}