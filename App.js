/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Button,Text,AsyncStorage, View,Image,ImageBackground,alignItems,resizeMode,styles,Dimensions,TextInput,TouchableOpacity} from 'react-native';
import {NavigationActions,StackActions} from 'react-navigation';
// import ValidateTextInput from "react-native-validate-textinput";
 

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
// this.props.navigation.navigate('')

const backAction =  StackActions.reset({
  index: 0,
  key: null,
  actions: [
  NavigationActions.navigate({routeName: 'Profile' })
  ]
  })
const {width,height}=Dimensions.get('screen');
export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      valueInput: "",
      email:'',
    password:'',
    isLoggingIn:false,
    message:'',
    };
  }

//   _onProcessTextChange(currentText){
//     if(!currentText){
//       this.setState({
//         errorText: 'Không được để trống'
//       })
//     } else if(!currentText.includes("@") && currentText){
//       this.setState({
//         errorText: 'Phải chứa ký tự @'
//       })
//     } else if(currentText.length < 8 && currentText){
//       this.setState({
//         errorText: 'Phải lớn hơn 8 ký tự'
//       })
//     }
//     else{
//       this.setState({
//         errorText: ''
//       })
//     }
// }
  static navigationOptions={
    header:null
  }
  _userLogin = async() => {

    this.setState({ isLoggingIn: true, message: '' });
  
    var params = {
        email: this.state.email,
        password: this.state.password
    };
   
    var formBody = [];
    for (var property in params) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(params[property]);
        formBody.push(encodedKey + ":" + encodedValue);
    }
    
    formBody = formBody.join("&");
  
    var proceed = false;
    var response = null;
    await fetch('http://f4a9e910.ngrok.io/snap_cart_backend/public/index.php/api/login', {
            method: "POST",
            headers: {
                'Accept':'applicaton/json',
                'Content-Type':'application/json',
                'Authorization':''
                // 'client-id':(Platform.OS==='ios')?'snap-app-ios':'snap-app-android',
  
            },
            body: JSON.stringify({
              email: params.email,
              password :params.password,
            })
        })
        .then((response) => response.json())
        .then((response) => {
            if (response.status==true) { 
              // this.props.navigation.navigate('profile')
                proceed = true;
                 var data = response.response;
                //  this.props.navigation.navigate('Profile');
               
                 // console.log(data);
          // alert('Data :       '+JSON.stringify(data));

                //  var accesstoken = data.access_token;
                 response=null;
               
                // // console.log(role);
                var user = JSON.stringify(data)
                AsyncStorage.setItem('user',user);
                // this.setState({'role': roler });
            }
            else {
          alert(JSON.stringify(response.error.message));

                this.setState({message:response.error.message});
                response=null;
        }
        })
        .then(() => {
            
            // AsyncStorage.getItem('user').then((value) => alert(value));
            this.setState({ isLoggingIn: false })
            if (proceed) {
                this.setState({isLoggedIn: true});
                
                this.props.navigation.dispatch(backAction);
                // this.props.navigation.dispatch(this.reset());
                
            }
        })
        .catch(err => {
          // alert('Invalid Input');
          alert('Catch Error :        '+JSON.stringify(err));
    this.setState({ message: err.message });
    this.setState({ isLoggingIn: false })
  });
  }
  render() {
    return (
      <View style={{flex:1}}>
      {/* <TouchableOpacity>gffdfgdhgd</TouchableOpacity> */}
        <ImageBackground source={require('./assets/Image/template1.jpg')} style={{flex:1,alignItems:'center',justifyContent:'center',}}resizeMode='stretch'>
        <TextInput style={{top:130,borderRadius:20,borderWidth:4,width:180,height:40}} onChangeText={(text)=>this.setState({email:text})} placeholder='Email'></TextInput>
        <TextInput style={{top:140,borderRadius:20,borderWidth:4,width:180,height:40}} secureTextEntry={true} onChangeText={(text)=>this.setState({password:text})}  placeholder='Password'></TextInput>

        {/* <ValidateTextInput
                  errorItem={this.state.errorText}
                  typeInput={"email"}
                  onChangeTextInput={(text) => {
                    this._onProcessTextChange(text);
                    this.setState({
                      valueInput: text
                    })
                  }}
                  hiddenIcon={false}
                  typeErrorView={"bottomInput"}
                  hiddenIconErrorView={true}
                />

<ValidateTextInput
                  errorItem={this.state.errorText}
                  typeInput={"password"}
                  onChangeTextInput={(text) => {
                    this._onProcessTextChange(text);
                    this.setState({
                      valueInput: text
                    })
                  }}
                  hiddenIcon={false}

                /> */}
        <TouchableOpacity 
      


      //  onPress={()=>this._userLogin()}
    

        style={{top:156,borderRadius:20,borderWidth:4,width:100,height:30}}>
        <Text style={{alignItems:'center',marginLeft:27,height:50}}onPress={()=>this._userLogin()}>Login</Text>
        </TouchableOpacity>
        <Text style={{top:200,marginLeft:-30}}>Have'nt shopped with SNAP cart before?</Text>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate('SignUp')} style={{top:182}}>
        <Text style={{alignItems:'center',marginLeft:280,fontWeight:'600',fontSize:15}}>SignUp</Text>
        </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}


