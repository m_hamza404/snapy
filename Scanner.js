import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Alert
} from 'react-native';
import Camera from 'react-native-camera';

export default class Scanner extends Component {
    static navigationOptions={
        header:null}

        state={

        }

    getInitialState() {
        return {
            scanning: true,
            cameraType: Camera.constants.Type.back
        }
        
}
onBarCodeRead(e) {
    // Alert.alert(
    //     "Barcode Found!",
    //     "Type: " + e.type + "\nData: " + e.data
    //     );
    fetch('http://f4a9e910.ngrok.io/snap_cart_backend/public/index.php/api/get_product?barcode='+e.data, {
        method: "GET",
        headers: {
            //'Accept':'applicaton/json',
            'Content-Type':'application/json',
           // 'Authorization':this.state.accesstoken,
            // 'client-id':'alnaqel-app-ios',
      
        }
      }).then(response => response.json())
      .then((response) => {
      // Alert.alert('',this.state.accesstoken);
        // this.setState({ isLoggingIn: false })
        if (response.status==true) { 
            // proceed = true; 
            // this.setState({isLoggedIn: false});
      
            //response ko notification ke state variable mein save karein ga
            // this.setState({notifications:response.response})
            // alert(response.response.name);

            this.props.navigation.state.params.additem(response.response);

            this.props.navigation.goBack();
            // Alert.alert(
            //     "Barcode Found!",
            //     "Type: " + e.type + "\nData: " + e.data
            //     );
      
      
            // this.setState({name:response.response.name,avatar:response.response.avatar,fav_dieticians:response.response.favourite_dieticians,user_dietician:response.response.user_dietician})
            // this.setState({imgList:[response.response.avatar,response.response.avatar_2,response.response.avatar_3],description:response.response.description,price:response.response.price+'',name:response.response.name,dietician_name:response.response.dietician_name})
            // this.setState({store:response.response});
        }
        // else if(response.status==false && response.error.custom_code==401)
        //       {
        //         // AsyncStorage.removeItem('accesstoken').then(() => RNRestart.Restart());
         
        //         // AsyncStorage.removeItem('role').then(() => (AsyncStorage.removeItem('profile')).then(() =>(AsyncStorage.removeItem('accesstoken')).then(() => RNRestart.Restart())));
        //       }
               else{
                Alert.alert('Alert',JSON.stringify(response.error.message ));
                // this.setState({new_trips:[]});
                // this.setState({ message: response.error.message });
      
                // this.setState({isLoggedIn: false});
                
               }          
      
      }
       ).catch(err => {
        Alert.alert('Alert',JSON.stringify(err.message ));
       
        //   this.setState({ message: err.message });
        //   this.setState({ isLoggedIn: false })
        });


    }
    
  
render() {
const { params} = this.props.navigation.state;

return (
<View style={styles.container}>
<Camera
ref={(cam) => {
this.camera = cam;
}}
onBarCodeRead={this.onBarCodeRead.bind(this)}
type={Camera.constants.Type.back0}
style={styles.preview}
aspect={Camera.constants.Aspect.fill}>
{/* <Text style={styles.capture} onPress={this.takePicture.bind(this)}>[CAPTURE]</Text> */}
</Camera>
</View>
);
}


}

const styles = StyleSheet.create({
container: {
flex: 1,
flexDirection: 'row',
},
preview: {
flex: 1,
justifyContent: 'flex-end',
alignItems: 'center'
},
capture: {
flex: 0,
backgroundColor: '#fff',
borderRadius: 5,
color: '#000',
padding: 10,
margin: 40
}
});

