import React, {Component} from 'react';
import {Platform,Alert,AsyncStorage, StyleSheet, Text, View,Image,ImageBackground,alignItems,resizeMode,styles,Dimensions,TextInput,TouchableOpacity} from 'react-native';
import {NavigationActions,StackActions} from 'react-navigation';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu'
});
const backAction =  StackActions.reset({
  index: 0,
  key: null,
  actions: [
  NavigationActions.navigate({routeName: 'Profile' })
  ]
  })
state ={
  name:'',
  email_address:'',
  password:'',
  phone:'',
  message: '',
  isLoggingIn: false,
  confirm_password:''
}


const {width,height}=Dimensions.get('screen');
export default class Profile extends Component {

    constructor(props){
        super(props);
        this.state={
          name:'',
          password:'',
          phone:'',
          email:''
        }
      }
    //   myFun=()=>{
    //       const {name,email_address,password,phone}=this.state;
    //       if(name==""){
    //           alert('name')
    //         }
    //         else if(email_address==""){
    //             alert('email')
    //         }
    //         else if(password==""){
    //             alert('pass')
    //         }
    //         else if(phone==""){
    //             alert('phone')
    //         }
    //         else{
    //             alert('seccess')
    //         }
    //   }
      

    static navigationOptions={
        header:null
      }
      _userSignUp = () => {

        this.setState({isLoggingIn: true,message: '' });
      
        var params = {
            name:this.state.name,
            email_address: this.state.email_address,
            password: this.state.password,
            phone:this.state.phone
        };
      
        // Alert.alert('Success','User successfully registered');
        var formBody = [];
        for (var property in params) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(params[property]);
            formBody.push(encodedKey + ":" + encodedValue);
        }
        formBody = formBody.join("&");
      
        var proceed = false;
        var response = null;
        if(true)
        {
        fetch('http://f4a9e910.ngrok.io/snap_cart_backend/public/index.php/api/signup', {
                method: "PUT",
                headers: {
                    // 'Accept':'applicaton/json',
                    'Content-Type':'application/json',
                    'Authorization':'',
                    // 'client-id':(Platform.OS==='ios')?'hilianspa-app-ios':'hilianspa-app-android',
      
                },
                body: JSON.stringify({
                    name: params.name,
                    email: params.email_address,
                    password :params.password,
                    phone: params.phone
                })
            })
            .then((response) => response.json())
            .then((response) => {
                if (response.status==true) { 
                    proceed = true;
                     var data = response.response;
                    //  alert(data);
                    //  var accesstoken = data.access_token;
                     response=null;
                    // // console.log(role);
                    // AsyncStorage.removeItem('accesstoken').then(() =>AsyncStorage.setItem('accesstoken',accesstoken));
                // AsyncStorage.setItem('user',JSON.stringify(response.response));

                    Alert.alert('User succesfully registered','Logged-in succesfully')
                    // this.setState({'role': roler });
                    // var data = response.response;
                    // // console.log(data);
                    // var roler = data.role;
                    // // console.log(role);
                    // AsyncStorage.setItem('role',roler);
                    // this.setState({'role': roler });
                }
                else {
                    //  this.setState({ message: response.error.message });
                    Alert.alert('Error ','ERRoR:      '+JSON.stringify(response.error.message));
            }
            })
            .then(() => {
                
                this.setState({ isLoggingIn: false })
                if (proceed) {
                    this.setState({isLoggedIn: true});
                    
                    this.props.navigation.dispatch(backAction);
                    // this.props.navigation.dispatch(this.reset());
                    
                }
            })
            .catch(err => {
          alert('Catch Error :        '+JSON.stringify(err));

                this.setState({ message: err.message });
                this.setState({ isLoggingIn: false })
            });
        }
        else{
            
            Alert.alert('Error','Password mismatch');
      
        }
      }
    render() {
        var navigation =this.props.navigation;
      return (
        <View style={{flex:1}}>
        <ImageBackground source={require('./assets/Image/template11.jpg')} style={{flex:1,alignItems:'center',justifyContent:'center',}}resizeMode='stretch'>
        <TextInput style={{top:50,borderRadius:20,borderWidth:4,width:150,height:40}} onChangeText={(text)=>this.setState({name:text})} placeholder='User name'></TextInput>
        <TextInput style={{top:70,borderRadius:20,borderWidth:4,width:150,height:40}} onChangeText={(text)=>this.setState({password:text})} placeholder='Password'></TextInput>
        <TextInput style={{top:80,borderRadius:20,borderWidth:4,width:150,height:40}} onChangeText={(text)=>this.setState({phone:text})} placeholder='Phone Number'></TextInput>
        <TextInput style={{top:100,borderRadius:20,borderWidth:4,width:150,height:40}} onChangeText={(text)=>this.setState({email_address:text})} placeholder='Email'></TextInput>
         <TouchableOpacity onPress={()=>this._userSignUp()} 
            //   onPress={()=>this.myFun} 
         style={{top:120,borderRadius:10,borderWidth:4,width:100,height:30}}>
        <Text style={{alignItems:'center',marginLeft:30}}>SignUp</Text>
        </TouchableOpacity>
         </ImageBackground>
        </View>
      );
    }}