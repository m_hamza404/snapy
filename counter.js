
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
 
Button,
Dimensions
} from 'react-native';

const {width,height} = Dimensions.get('window');

export default class Counter extends Component
{  
    state={
        count:1,
        }

        onButtonPress(text)
        {
            if(text>=1)
            {
            this.setState({count:text});
            this.props.getValue(text);
            }
            else
            {
            this.props.getValue(1);
            }
            // return text;
        }
    render(){
        return(
        <View style={{height:20,width:30,flexDirection:'row',justifyContent:'space-between', textAlign:'right',alignItems:'center'}}>
           
            <Button
              onPress={() =>this.onButtonPress(this.state.count+1)} title='+'>
              
            </Button>
            <Text style={{textAlign:'right'}}>{this.state.count}</Text>
        
            <Button
              onPress={() =>this.onButtonPress(this.state.count-1) } title="-">
            </Button>
            </View> 

        )};
}
