import React, {Component} from 'react';
import {Platform, StyleSheet, Text,Alert, View,FlatList,Image,ScrollView,ImageBackground,AsyncStorage,alignItems,resizeMode,Dimensions,TextInput,TouchableOpacity} from 'react-native';

// const Item = 
const {width,height}=Dimensions.get('screen');
export default class MPreviouscart extends Component {
    static navigationOptions={
        header:null
      }
      state = {
        list: [],
        text: "",
        breadlist:[],
        dairy:[],
        meats:[],
        seafood:[],
        pasta:[],
        frozen:[]
      };
    
      changeTextHandler = text => {
        this.setState({ text: text });
      };
    
    
      _getlist = async()=>{
        try{
           let list =   await AsyncStorage.getItem("list")
          list = (list == null)?[]:JSON.parse(list)
          this.setState({list:list})
        }
        catch(error){
    
        }
      }

      
  filterlist= async (id)=>{
    
    let list = this.state.list
     list = list.filter((item)=>{
       return item.id !== id
     })
     this.setState({list:list})
     try{
       await AsyncStorage.setItem('list',JSON.stringify(this.state.list))
     }
     catch(error){
 
     }
   }

      componentDidMount(){
        this._getlist();
      }
    render() {
        var navigation =this.props.navigation;
      return (
        <View style={{flex:1,backgroundColor:'rgba(0,0,0,0.1)',alignItems:'center'}}>
         <Text style={{alignItems:'center',fontWeight:"bold",fontSize:30,}}>Shooping List</Text>
         <ScrollView>
       {/* <Text style={{fontSize:18,fontFamily:'Roboto-Black',color:'rgba(0,0,0,0.7)',marginTop:(this.state.list==0)?30:0,textAlign:'center',fontWeight:'bold',textAlignVertical:'bottom'}}>{(this.state.list.length < 1 )?'Empty List':''}</Text> */}
       <FlatList
        numColumns={2}
        showsVerticalScrollIndicator={false}
         style={styles.list}
          data={this.state.list}
         
          keyExtractor={(item)=>item.id.toString()}
          contentContainerStyle={{alignItems:'center',justifyContent:'center'}}
          ListFooterComponent={()=><View style={{alignItems:'center',justifyContent:'center',marginBottom:30,width:width,height:100}}>
          <TouchableOpacity style={{alignSelf:'center',alignItems:'center',justifyContent:'center',width:170,height:60,borderRadius:30,backgroundColor:'rgba(0,0,0,0.6)',marginTop:(this.state.list.length < 1)?30:10}} onPress={() => {this.props.navigation.navigate('MProductslist',{refresh:this._getlist,id:navigation.state.params.id})}}><Text style={{color:'#FFFFFF'}}>Add Shopping List</Text></TouchableOpacity></View>}
          renderItem={({ item, index }) =>
            // <View>
            //   <View style={styles.listItemCont}>
            //     <Text style={styles.listItem}>
            //       {item.text}
            //     </Text>
            //     <Button title="X" onPress={() => this.deleteTask(index)} />
            //   </View>
            //   <View style={styles.hr} />
            // </View>(navigation.state.params.id=='scan')?
            <TouchableOpacity onLongPress={()=>{Alert.alert(   // Shows up the alert without redirecting anywhere
                'Remove '+item.name
                , ''
                , [
                    { text: 'Yes', onPress: () => { this.filterlist(item.id)} },
                    { text: 'No' }
                ]
            );
            }
            } onPress={()=>{this.props.navigation.navigate('MNewcart',{header:'Dairy and eggs',list:item,refresh:this._getlist,id:navigation.state.params.id})}}  
        style={{width:(width-50)/2,marginVertical:10,marginHorizontal:10,height:110,backgroundColor:'#FFFFFF',alignSelf:'center',shadowColor:'#C4C4C4',shadowOpacity:0.2,shadowOffset:{width:0,height:13},elevation:5,shadowRadius: 6}}>
        <View style={{width:80,height:4,backgroundColor:'rgba(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',0.7)'}}></View>
        <View style={{flex:1,justifyContent:'center'}}>
        {/* <Image source={item.avatar?{uri:item.avatar}:require('./assets/default.png')} resizeMode='contain' style={{marginLeft:20,width:35,height:45,marginBottom:7}}></Image> */}
        <Text style={{marginLeft:19,fontSize:15,color:'rgba(0,0,0,0.55)'}}>{item.name}</Text>
        </View>
        {/* </TouchableOpacity>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Add_Shopping_Items',{header:'Dairy and eggs'})}} style={{width:(width-50)/2,height:130,backgroundColor:'#FFFFFF'}}>
            <View style={{width:80,height:4,backgroundColor:'rgba(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',0.7)'}}></View>
            <Text>{item.text}</Text> */}
            </TouchableOpacity>
          
          }
        />
         
      {/* <ScrollView style={styles.main} contentContainerStyle={{alignItems: 'center'}}>
         <View style={{width:width-30,marginTop:15,justifyContent:'space-between',alignItems:'center',height:140,backgroundColor:'transparent',flexDirection:'row'}}>
         <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Add_Shopping_Items',{header:'Breads and grains'})}} style={{width:(width-50)/2,height:130,backgroundColor:'#FFFFFF'}}>
        <View style={{width:80,height:4,backgroundColor:'#D35D4E'}}></View>
         <Text>Breads and grains</Text>
         </TouchableOpacity>
         <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Add_Shopping_Items',{header:'Dairy and eggs'})}} style={{width:(width-50)/2,height:130,backgroundColor:'#FFFFFF'}}>
         <View style={{width:80,height:4,backgroundColor:'#8AC9FC'}}></View>
         <Text>Dairy and eggs</Text>
         </TouchableOpacity>
         </View>
         <View style={{width:width-30,marginTop:15,justifyContent:'space-between',alignItems:'center',height:140,backgroundColor:'transparent',flexDirection:'row'}}>
         <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Add_Shopping_Items',{header:'Meats'})}} style={{width:(width-50)/2,height:130,backgroundColor:'#FFFFFF'}}>
         <View style={{width:80,height:4,backgroundColor:'#D35D4E'}}></View>
         <Text>Meats</Text>
         </TouchableOpacity>
         <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Add_Shopping_Items',{header:'Seafood'})}} style={{width:(width-50)/2,height:130,backgroundColor:'#FFFFFF'}}>
         <View style={{width:80,height:4,backgroundColor:'#8AC9FC'}}></View>
         <Text>Seafood</Text>
         </TouchableOpacity>
         </View>
         <View style={{width:width-30,marginTop:10,marginBottom:10,justifyContent:'space-between',alignItems:'center',height:140,backgroundColor:'transparent',flexDirection:'row'}}>
         <TouchableOpacity  onPress={()=>{this.props.navigation.navigate('Add_Shopping_Items',{header:'Pasta and Rice'})}} style={{width:(width-50)/2,height:130,backgroundColor:'#FFFFFF'}}>
         <View style={{width:80,height:4,backgroundColor:'#F5CE8D'}}></View>
         <Text>Pasta and Rice</Text>
         </TouchableOpacity>
         <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Add_Shopping_Items',{header:'Frozen items'})}} style={{width:(width-50)/2,height:130,backgroundColor:'#FFFFFF'}}>
         <View style={{width:80,height:4,backgroundColor:'#F49697'}}></View>
         <Text>Frozen items</Text>
         </TouchableOpacity>
         </View>
         <TouchableColored onPress={() => {this.props.navigation.navigate('Add_Shopping')}}><Text style={{fontFamily:'Roboto-Medium',color:'#FFFFFF'}}>Add Shopping List</Text></TouchableColored>
       
         </ScrollView> */}
         </ScrollView>
               </View>
      );
    }}
    
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#F5FCFF",
    //   padding: viewPadding,
      paddingTop: 20
    },
    list: {
      width: "100%",
      // backgroundColor:'#000000',
      marginTop:10
    },
    listItem: {
      paddingTop: 2,
      paddingBottom: 2,
      fontSize: 18
    },
    hr: {
      height: 1,
      backgroundColor: "gray"
    },
    listItemCont: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between"
    },
    textInput: {
      height: 40,
      paddingRight: 10,
      paddingLeft: 10,
      borderColor: "gray",
      borderWidth: 1,
      width: "100%"
    }
  });