import React, {Component} from 'react';
// import {Platform, StyleSheet, Text, View,Image,ImageBackground,alignItems,resizeMode,styles,Dimensions,TextInput,TouchableOpacity} from 'react-native';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,ImageBackground,Dimensions,TextInput,ScrollView,TouchableOpacity,FlatList,AsyncStorage,Alert
} from 'react-native';

const {width,height}=Dimensions.get('screen');
export default class MNewcart extends Component {
  state = {
    isOpen: false,
    isDisabled: false,
    swipeToClose: true,
    sliderValue: 0.3,
    openmodal:false,
    language:null,
    weightSelected:1,
    heightSelected:1,
    selectedIndex: 0,
    selectedIndex2: 0,
    list:this.props.navigation.state.params.list,
    name:this.props.navigation.state.params.list.name,
    itemname:'',
    items:this.props.navigation.state.params.list.items,
    avatar:this.props.navigation.state.params.list.avatar,
    steps:0,
    distance:0,
    user:{},
    caloriesburned:0,
    gender: [
      {id:null, name:'Gender'},
      {id:1, name: 'Male'},
      {id:2, name: 'Female'}
  ],
  weight: [
    {id:1, name: 'kgs'},
    {id:2, name: 'lb'},
],height: [
  {id:1, name: 'm'},
  {id:2, name: 'ft'},
  {id:3, name: 'in'},
],
  }
    static navigationOptions={
        header:null
      }
      componentDidMount=async()=>{
        await AsyncStorage.getItem('user').then((value) => {this.setState({user:JSON.parse(value)})});
      }
      additem=async(item)=>{
        
        if(this.props.navigation.state.params.custom)
        {

          this.save();
        }

        else{        
        let items1 = this.state.items
        items1.unshift({id:Date.now(),p_id:item.prduct_id,quantity:1,name:item.name,description:item.description,price:item.price})
        
        this.setState({items:items1,itemname:""})
        try{
          let list  = await AsyncStorage.getItem("list")
          list = list ? JSON.parse(list) :[]
          objIndex = list.findIndex((item => item.id == this.state.list.id));
          list[objIndex].items = items1
          
       //    list.unshift({id:Date.now(),name:this.state.listname,items:this.state.items})
          await AsyncStorage.setItem("list",JSON.stringify(list))
          // alert('done')
       
           }
           catch(error){
             alert(error)
           }
          }
      }
      filteritem = async(id)=>{
        
        let items = this.state.items
        items = items.filter((item)=>{
          return item.id !== id
        })
        this.setState({items:items})
        try{
          let list  = await AsyncStorage.getItem("list")
          list = list ? JSON.parse(list) :[]
          objIndex = list.findIndex((item => item.id == this.state.list.id));
          list[objIndex].items = items
          
       //    list.unshift({id:Date.now(),name:this.state.listname,items:this.state.items})
          await AsyncStorage.setItem("list",JSON.stringify(list))
          // alert('done')
       
          this.props.navigation.state.params.refresh()
           }
           catch(error){
             alert(error)
           }
      }
      save = async()=>{
        try{
          let list  = await AsyncStorage.getItem(this.props.navigation.state.params.list.name)
          list = list ? JSON.parse(list) :[]
          
          list.unshift({id:Date.now(),name:this.state.itemname,items:this.state.items,avatar:this.state.avatar})
          await AsyncStorage.setItem(this.props.navigation.state.params.list.name,JSON.stringify(list)).then(()=>{
            this.setState({items:list,itemname:""})
   
           this.props.navigation.state.params.refresh();
          //  this.props.navigation.goBack();
          })
         //  alert('done')
           }
           catch(error){
             alert(error)
           }
          }


          _checkout = () => {

            this.setState({isLoggingIn: true,message: '' });
          
            var params = {
                user_id:this.state.user.user_id,
                product_ids:this.state.items
            };
          
            // Alert.alert('Success','User successfully registered');
            var formBody = [];
            for (var property in params) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(params[property]);
                formBody.push(encodedKey + ":" + encodedValue);
            }
            formBody = formBody.join("&");
          
            var proceed = false;
            var response = null;
            if(true)
            {
            fetch('http://f4a9e910.ngrok.io/snap_cart_backend/public/index.php/api/book/cart', {
                    method: "POST",
                    headers: {
                        // 'Accept':'applicaton/json',
                        'Content-Type':'application/json',
                        'Authorization':'',
                        // 'client-id':(Platform.OS==='ios')?'hilianspa-app-ios':'hilianspa-app-android',
          
                    },
                    body: JSON.stringify({
                        user_id: params.user_id,
                        product_ids: params.product_ids
                    })
                })
                .then((response) => response.json())
                .then((response) => {
                    if (response.status==true) { 
                        proceed = true;
                        //  var data = response.response;
                         // console.log(data);
                        //  var accesstoken = data.access_token;
                         response=null;
                        // // console.log(role);
                        // AsyncStorage.removeItem('accesstoken').then(() =>AsyncStorage.setItem('accesstoken',accesstoken));
                        // Alert.alert('User succesfully registered','Logged-in succesfully')
                        this.props.navigation.goBack();
                        // this.setState({'role': roler });
                        // var data = response.response;
                        // // console.log(data);
                        // var roler = data.role;
                        // // console.log(role);
                        // AsyncStorage.setItem('role',roler);
                        // this.setState({'role': roler });
                    }
                    else {
                        //  this.setState({ message: response.error.message });
                        Alert.alert('Error ','ERRoR:      '+JSON.stringify(response.error.message));
                }
                }).catch(err => {
              alert('Catch Error :        '+JSON.stringify(err));
    
                    this.setState({ message: err.message });
                    this.setState({ isLoggingIn: false })
                });
            }
            else{
                
                // Alert.alert('Error','Password mismatch');
          
            }
          }


    render() {
        var navigation =this.props.navigation;
      return (
        <View style={{flex:1,justifyContent:'center'}}>
        
         
        <ScrollView style={styles.main} contentContainerStyle={{alignItems: 'center'}}>
        <View style={{width:width-30,marginTop:15,justifyContent:'flex-start',alignItems:'center',height:60,backgroundColor:'transparent',flexDirection:'row'}}>
        <View style={{width:60,height:60,borderRadius:30,backgroundColor:'#FFFFFF',justifyContent:"center",alignItems:'center'}}>
        <View  style={{width:60,height:60,borderRadius:30,justifyContent:"center",alignItems:'center'}} >
        {/* // onPress={()=>{
        //   ImagePicker.showImagePicker(options, (response) => {
        //     console.log('Response = ', response);
          
        //     if (response.didCancel) {
        //       console.log('User cancelled image picker');
        //     } else if (response.error) {
        //       console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //       console.log('User tapped custom button: ', response.customButton);
        //     } else {
        //       const source = { uri: response.uri };
          
        //       // You can also display the image using data:
        //       // const source = { uri: 'data:image/jpeg;base64,' + response.data };
          
        //       this.setState({
        //         avatar:'data:image/jpeg;base64,'+response.data,
        //       });
        //     }
        //   });
        // }}> */}
        {/* <Image style={{width:56,height:56,borderRadius:28}} resizeMode={this.state.avatar?'contain':'center'} source={(this.props.navigation.state.params.custom)?this.props.navigation.state.params.image:this.state.avatar? {uri:this.state.avatar}:require('./assets/camera3.png')}></Image> */}
        </View>
        </View>
        <View style={{backgroundColor:'transparent',marginLeft:15}}><Text style={{color:'#000000'}}>{this.state.name}</Text>
        <Text style={{fontSize:10}}>{this.state.items.length} Items</Text>
        </View>
        </View>
        <View style={{flexDirection:'row'}}>
        <TouchableOpacity style={{alignSelf:'flex-end',alignItems:'center',justifyContent:'center',width:140,height:50,borderRadius:30,backgroundColor:'red',marginTop:10}}  onPress={()=>{this._checkout()}}><Text style={{color:'#FFFFFF'}}>Checkout</Text></TouchableOpacity>

          </View>
          {/* <View style={{width:width-30,marginTop:20,marginBottom:25}}>
          <Text style={{color:'#6DC9BE',marginBottom:7}}>Add New Item</Text>
          <TextInput
                                                style={{  paddingLeft:10,padding: 0,height:40, fontSize:12,backgroundColor:'#FFFFFF' ,fontFamily: 'Roboto-Medium' }}
                                                placeholder=" Add New Item..."
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                placeholderTextColor="grey"
                                                value={this.state.itemname}
                                                onChangeText={(text)=>{this.setState({itemname:text})}}

                                            />
          </View> */}
        <TouchableOpacity style={{alignSelf:'center',alignItems:'center',justifyContent:'center',width:140,height:50,borderRadius:30,backgroundColor:'#6DC9BE',marginTop:10}}  onPress={()=>{(navigation.state.params.id=='scan')?this.props.navigation.navigate('Scanner',{additem:(item) => this.additem(item)}):{}}}><Text style={{color:'#FFFFFF'}}>{(navigation.state.params.id=='scan')?'SCAN':'ADD ITEM'}</Text></TouchableOpacity>

        {/* <TouchableColored disabled={this.state.itemname?false:true} onPress={this.additem} ><Text style={{fontFamily:'Roboto-Medium',color:'#FFFFFF'}}>Add Item</Text></TouchableColored> */}
       
        <Text style={{color:'#6DC9BE',alignSelf:'flex-start',marginTop:20,marginLeft:15}}>Total Items ({this.state.items.length})</Text>
       <View style={{width:width-30,marginTop:10,paddingBottom:20,backgroundColor:'#FFFFFF',justifyContent:'center',alignItems:'center'}}>
       <FlatList 
       data={this.state.items}
        renderItem={({item})=><View style={{height:50,marginTop:20,width:width-80,borderBottomWidth:0.7,borderBottomColor:'rgba(0,0,0,0.2)',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
        <View style={{justifyContent:'flex-start',alignItems:'flex-start',flexDirection:'column'}}>
        {/* <Image source={require('./assets/bullets.png')} style={{width:15,height:15,marginRight:10}}></Image> */}
        <View style={{flexDirection:'row'}}>
        <Text style={{fontSize:15,fontWeight:'bold'}}>Product: </Text>
        <Text>{item.name}</Text>
        </View>
        <View style={{flexDirection:'row'}}>
        <Text style={{fontSize:15,fontWeight:'bold'}}>Price: </Text>
        <Text style={{marginRight:5}}>{'Rs.'+item.price+''}</Text>
        </View>


        </View>
        {(this.props.navigation.state.params.custom)?<View></View>:
        <TouchableOpacity onPress={()=>{this.filteritem(item.id)}}>
        <Text>Remove</Text>
        {/* <Image source={require('./assets/cross.png')} resizeMode='center' style={{width:14,height:14}}></Image> */}
        </TouchableOpacity>}
        </View>}
        keyExtractor={(item)=>item.id.toString()}
        extraData={this.state}/>
       
       </View>
        </ScrollView>
         
               </View>
      );
    }}
    const styles = StyleSheet.create({
      wrapper: {
      },
      modal: {
        flex:1,
        borderWidth:0
      },
      main: {
        flex: 1,
        flexDirection:'column',
        backgroundColor: 'transparent',
      },
      text: {
        color: '#fff',fontFamily:'Roboto-Medium',
        fontSize: 30,
        fontWeight: 'bold',
        textAlign:'center',
        marginBottom: 7
      },
      textsub:{
        color: '#fff',fontFamily:'Roboto-Medium',
        fontSize: 17,
        textAlign:'center',
        fontWeight: 'bold',
      },
      dot:{
        backgroundColor:'rgba(0,0,0,.2)',
        width:53,
        height:7,
        marginLeft:6,
        marginRight:6,
        marginTop:5,
        marginBottom:3
      },
      activedot:{
        backgroundColor:'rgba(255,255,255,.8)',
        width:53,
        height:7,
        marginLeft:6,
        marginRight:6,
        marginTop:5,
        marginBottom:3
      },
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,fontFamily:'Roboto-Medium',
        textAlign: 'center',
        margin: 10,
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
    });
    