
import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,ImageBackground,Dimensions,TextInput,ScrollView,TouchableOpacity,FlatList,AsyncStorage,Alert
} from 'react-native';
// import {Platform, StyleSheet, Text, View,Image,ImageBackground,alignItems,resizeMode,styles,Dimensions,TextInput,TouchableOpacity,FlatList} from 'react-native';
import Counter from './counter';
import Spinner from 'react-native-number-spinner';


const {width,height}=Dimensions.get('screen');
export default class MProductslist extends Component {
    static navigationOptions={
        header:null
      }
      constructor(props, context) {
        super(props, context);
        this.state = {
          isOpen: false,
          listname:"",
      itemname:"",
      items:[],
      avatar:'',
      name:'',
          isDisabled: false,
          swipeToClose: true,
          sliderValue: 0.3,
          openmodal:false,
          language:null,
          weightSelected:1,
          heightSelected:1,
          selectedIndex: 0,
          selectedIndex2: 0,
          steps:0,
          distance:0,
          caloriesburned:0,
          gender: [
            {id:null, name:'Gender'},
            {id:1, name: 'Male'},
            {id:2, name: 'Female'}
        ],
        weight: [
          {id:1, name: 'kgs'},
          {id:2, name: 'lb'},
      ],height: [
        {id:1, name: 'm'},
        {id:2, name: 'ft'},
        {id:3, name: 'in'},
    ],
        }

        // this.onValueChange = this.handleValueChange.bind(this)
        // this.onValueChangeWeightSelected = this.handleValueChangeWeightSelected.bind(this)
        // this.onValueChangeHeightSelected = this.handleValueChangeHeightSelected.bind(this)
        // this.updateIndex = this.updateIndex.bind(this)
        // this.updateIndex2 = this.updateIndex2.bind(this)
      }

      
      componentDidMount=async()=>{
        await AsyncStorage.getItem('user').then((value) => {this.setState({user:JSON.parse(value)})});
      }


    additem= async(item)=>{
        
      let items1 = this.state.items
     await items1.unshift({id:Date.now(),p_id:item.prduct_id,quantity:1,name:item.name,description:item.description,price:item.price})
      
      this.setState({items:items1,itemname:""})
    }
    save = async()=>{
  
      try{
     let list  = await AsyncStorage.getItem("list")
     list = list ? JSON.parse(list) :[]
     
     list.unshift({id:Date.now(),name:this.state.listname,items:this.state.items,avatar:this.state.avatar})
     await AsyncStorage.setItem("list",JSON.stringify(list)).then(()=>{

      this.props.navigation.state.params.refresh();
      // this.props.navigation.goBack();
      this._checkout();
     })
    //  alert('done')
      }
      catch(error){
        alert(error)
      }
    }
    filteritem = (id)=>{
      
      let items = this.state.items
      items = items.filter((item)=>{
        return item.id !== id
      })
      this.setState({items:items})
    }


    _checkout = () => {

      this.setState({isLoggingIn: true,message: '' });
    
      var params = {
          user_id:this.state.user.user_id,
          product_ids:this.state.items
      };
    
      // Alert.alert('Success','User successfully registered');
      var formBody = [];
      for (var property in params) {
          var encodedKey = encodeURIComponent(property);
          var encodedValue = encodeURIComponent(params[property]);
          formBody.push(encodedKey + ":" + encodedValue);
      }
      formBody = formBody.join("&");
    
      var proceed = false;
      var response = null;
      if(true)
      {
      fetch('http://f4a9e910.ngrok.io/snap_cart_backend/public/index.php/api/book/cart', {
              method: "POST",
              headers: {
                  // 'Accept':'applicaton/json',
                  'Content-Type':'application/json',
                  'Authorization':'',
                  // 'client-id':(Platform.OS==='ios')?'hilianspa-app-ios':'hilianspa-app-android',
    
              },
              body: JSON.stringify({
                  user_id: params.user_id,
                  product_ids: params.product_ids
              })
          })
          .then((response) => response.json())
          .then((response) => {
              if (response.status==true) { 
                  proceed = true;
                  //  var data = response.response;
                   // console.log(data);
                  //  var accesstoken = data.access_token;
                   response=null;
                  // // console.log(role);
                  // AsyncStorage.removeItem('accesstoken').then(() =>AsyncStorage.setItem('accesstoken',accesstoken));
                  // Alert.alert('User succesfully registered','Logged-in succesfully')
                  this.props.navigation.goBack();
                  // this.setState({'role': roler });
                  // var data = response.response;
                  // // console.log(data);
                  // var roler = data.role;
                  // // console.log(role);
                  // AsyncStorage.setItem('role',roler);
                  // this.setState({'role': roler });
              }
              else {
                  //  this.setState({ message: response.error.message });
                  Alert.alert('Error ','ERRoR:      '+JSON.stringify(response.error.message));
          }
          }).catch(err => {
        alert('Catch Error :        '+JSON.stringify(err));

              this.setState({ message: err.message });
              this.setState({ isLoggingIn: false })
          });
      }
      else{
          
          // Alert.alert('Error','Password mismatch');
    
      }
    }




    render() {
      var navigation =this.props.navigation;
        
      return (
        <View style={{flex:1,backgroundColor:'rgba(0,0,0,0.1)',alignItems:'center'}}>
        
        <ScrollView style={styles.main} contentContainerStyle={{alignItems: 'center'}}>
        {/* <View style={{width:60,height:60,marginTop:15,borderRadius:30,backgroundColor:'#FFFFFF',justifyContent:"center",alignItems:'center'}}>
        <TouchableOpacity  style={{width:60,height:60,borderRadius:30,justifyContent:"center",alignItems:'center'}} onPress={()=>{
          ImagePicker.showImagePicker(options1, (response) => {
            console.log('Response = ', response);
          
            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {
              const source = { uri: response.uri };
          
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };
          // console.warn(response.data);
              this.setState({
                avatar:'data:image/jpeg;base64,'+response.data,
              });
            }
          });
        }}>
        <Image style={{width:56,height:56,borderRadius:28}} resizeMode={this.state.avatar?'cover':'center'} source={this.state.avatar? {uri:this.state.avatar}:require('./assets/camera3.png')}></Image>
        </TouchableOpacity>
        </View> */}
          <View style={{width:width-30,marginTop:7,marginBottom:10}}>
          <Text style={{color:'#6DC9BE',marginBottom:7}}>Name</Text>
          <TextInput
                                                style={{ paddingLeft:10,padding: 0,height:40, fontSize:12,backgroundColor:'#FFFFFF' }}
                                                placeholder=" Enter Shopping List Name..."
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                placeholderTextColor="grey"
                                                value={this.state.listname}
                                                onChangeText={(text)=>{this.setState({listname:text})}}

                                            />
          </View>
          {/* <View style={{width:width-30,marginBottom:25}}>
          <Text style={{color:'#6DC9BE',marginBottom:7}}>Add New Item</Text>
          <TextInput
                                                style={{ paddingLeft:10,padding: 0,height:40, fontSize:12,backgroundColor:'#FFFFFF' }}
                                                placeholder=" Add New Item..."
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                placeholderTextColor="grey"
                                                value={this.state.itemname}
                                                onChangeText={(text)=>{this.setState({itemname:text})}}

                                            />
          </View> */}
        <TouchableOpacity style={{alignSelf:'center',alignItems:'center',justifyContent:'center',width:140,height:50,borderRadius:30,backgroundColor:'#6DC9BE',marginTop:10}} onPress={()=>{(navigation.state.params.id=='scan')?this.props.navigation.navigate('Scanner',{additem:(item) => this.additem(item)}):{}}}><Text style={{color:'#FFFFFF'}}>{(navigation.state.params.id=='scan')?'SCAN':'ADD ITEM'}</Text></TouchableOpacity>
        {/* <TouchableOpacity disabled={this.state.itemname?false:true} onPress={this.additem}><Text style={{color:'#FFFFFF'}}>Add Item</Text></TouchableOpacity> */}
       
        <Text style={{color:'#6DC9BE',alignSelf:'flex-start',marginTop:20,marginBottom:10,marginLeft:15}}>Total Items ({this.state.items.length})</Text>
        <FlatList
        data={this.state.items}
        renderItem={({item})=>
       <View style={{width:width-30,marginTop:2,marginBottom:2,backgroundColor:'#FFFFFF',justifyContent:'center',alignItems:'center',alignSelf:'center'}}>
       <View style={{height:50,marginTop:20,marginBottom:20,width:width-80,borderBottomWidth:0.7,borderBottomColor:'rgba(0,0,0,0.2)',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
       <View style={{justifyContent:'flex-start',alignItems:'flex-start',flexDirection:'column'}}>
        {/* <Image source={require('./assets/bullets.png')} style={{width:15,height:15,marginRight:10}}></Image> */}
        <View style={{flexDirection:'row'}}>
        <Text style={{fontSize:15,fontWeight:'bold'}}>Product: </Text>
        <Text>{item.name}</Text>
        </View>
        <View style={{flexDirection:'row'}}>
        <Text style={{fontSize:15,fontWeight:'bold'}}>Price: </Text>
        <Text style={{marginRight:5}}>{'Rs.'+item.price+''}</Text>
        </View>
        </View>
       <TouchableOpacity onPress={()=>{this.filteritem(item.id)}}>
       <Text>Remove</Text>
       {/* <Image source={require('./assets/cross.png')} resizeMode='center' style={{width:14,height:14}}></Image> */}
       </TouchableOpacity>
       </View>
       </View>}
       keyExtractor={(item)=>item.id.toString()}
       extraData={this.state}
       ListFooterComponent={()=><View style={{alignItems:'center',justifyContent:'center',marginBottom:30,width:width,height:100}}>
       <TouchableOpacity style={{alignSelf:'center',alignItems:'center',justifyContent:'center',width:170,height:60,borderRadius:30,backgroundColor:'rgba(0,0,0,0.6)',marginTop:10}} onPress={this.save}><Text style={{fontFamily:'Roboto-Medium',color:'#FFFFFF'}}>Save & Checkout</Text></TouchableOpacity>
       </View>}
       />
        
        </ScrollView>

                {/* <Text style={{fontSize:30,fontWeight:'bold',fontFamily:'Penna'}}>Products List</Text>
        <FlatList
     style={{flex:1,marginTop:6}} 
     data={this.state.items}
     keyExtractor={(item)=>item.key}
    renderItem={({item})=>
    <View style={{width:width-30,height:80,marginVertical:6,borderBottomColor:'#000000',borderBottomWidth:0.75,flexDirection:'column',alignItems:'flex-start'}}>
    <Text>{item.name}</Text>
    <Text>{item.description}</Text>
    <View style={{flexDirection:'row',marginLeft:140,borderColor:'#000000'}}> */}
   
    {/* Counter button  */}
    {/* <Counter getValue={(text)=>{ this.state.data[item.id].count = text;this.forceUpdate() }} ></Counter> */}
    {/* <Spinner max={10}
         min={2}
         default={5}
         color="#f60"
         numColor="#f60"
         onNumChange={(num)=>{console.log(num)}} />
    
    {/* Delete Button */}
    {/* <TouchableOpacity style={{backgroundColor:'#FF4500',borderRadius:20,borderColor:'#000000',marginLeft:10,width:70,alignItems:'center'}}>
      <Text style={{fontSize:15,fontWeight:'bold'}}>Delete</Text>
    </TouchableOpacity>
    </View>
    </View>
     }
    extraData={this.state.items}
    >

         </FlatList>  

         
         <TouchableOpacity onPress={()=>this.props.navigation.navigate('Scanner')}>
           <Text style={{fontSize:20,fontWeight:"bold" ,borderRadius:8,backgroundColor:'#808080',width:150,justifyContent:'center',alignItems:'center',alignContent:'center'}}>+ ADD NEW </Text>
         </TouchableOpacity> */}
</View>
      );
    }}
    const styles = StyleSheet.create({
      wrapper: {
      },
      modal: {
        flex:1,
        borderWidth:0
      },
      main: {
        flex: 1,
        flexDirection:'column',
        backgroundColor: 'transparent',
      },
      text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
        textAlign:'center',
        marginBottom: 7
      },
      textsub:{
        color: '#fff',
        fontSize: 17,
        textAlign:'center',
        fontWeight: 'bold',
      },
      dot:{
        backgroundColor:'rgba(0,0,0,.2)',
        width:53,
        height:7,
        marginLeft:6,
        marginRight:6,
        marginTop:5,
        marginBottom:3
      },
      activedot:{
        backgroundColor:'rgba(255,255,255,.8)',
        width:53,
        height:7,
        marginLeft:6,
        marginRight:6,
        marginTop:5,
        marginBottom:3
      },
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
      },
    });